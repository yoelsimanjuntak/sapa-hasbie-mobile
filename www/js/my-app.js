var $$ = Dom7;
var app = new Framework7({
  // App root element
  el: '#app',
  theme:'md',
  // App Name
  name: 'SAPA HASBIE',
  // App id
  id: 'io.partopitao.sitortor',
  // Enable swipe panel
  panel: {
    swipe: true,
  },
  routes: routes
});
var viewHome = app.views.create('#view-home', {name:'home', main:true});
var viewChat = app.views.create('#view-chat', {name:'chat'});
var viewProfile = app.views.create('#view-profile', {name:'profile'});
//var viewNotifikasi = app.views.create('#view-notification', {name:'notification'});
var objMessagebar = app.messagebar.create({
  el: '.messagebar[data-label=messagebar-chat]'
});
var objMessages = app.messages.create({
  el: '.messages[data-label=list-chat]',
  // First message rule
  firstMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no previous message
      - or previous message type (send/received) is different
      - or previous message sender name is different
    */
    if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  lastMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if:
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  tailMessageRule: function (message, previousMessage, nextMessage) {
    // Skip if title
    if (message.isTitle) return false;
    /* if (basically same as lastMessageRule):
    - there is no next message
    - or next message type (send/received) is different
    - or next message sender name is different
  */
    if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
    return false;
  },
  // Last message rule
  sameFooterMessageRule: function (message, previousMessage, nextMessage) {
    if (message.isTitle) return false;
    if (nextMessage && nextMessage.type == message.type && nextMessage.name == message.name) return true;
    return false;
  }
});

function _loadChats(messages_) {
  var msgCurrent = getLocalStorage('chats');
  var msgNew = [];

  if(!messages_ || messages_.length == 0) {
    objMessages.clear();
    removeLocalStorage('chats');
    return;
  }

  if(msgCurrent) {
    for(var i=0; i<messages_.length; i++) {
      if(messages_[i].attrs) {
        var exist_ = msgCurrent.find(x => x.attrs.PesanID == messages_[i].attrs.PesanID);
        if(!exist_) {
          msgNew.push(messages_[i]);
        }
      }
    }

    if(msgNew && msgNew.length>0) {
      msgCurrent = msgCurrent.concat(msgNew);
    }
  } else {
    objMessages.clear();
    msgNew = messages_;
    msgCurrent = messages_;
  }

  setLocalStorage('chats', msgCurrent);
  //console.log('new', msgNew);
  if(objMessages.messages.length > 0) {
    objMessages.addMessages(msgNew);
  } else {
    objMessages.addMessages(msgCurrent);
  }
}

$(document).on('click', '.route-tab-link', function (e) {
  var url = $(this).attr('href');
  var view = app.views.get($(this).attr('data-view'));
  var tabId = $(this).attr('data-tab');
  app.tab.show(tabId);
  view.router.navigate(url);
});

/*viewHome.on('view:init', function(){});
viewProject.on('view:init', function(){});
viewProfile.on('view:init', function(){});*/

var auth = getLocalStorage('auth');
//$$('a.tab-link[href="#view-chat"]').show();
//$$('a.tab-link[href="#view-notification"]').hide();
if (auth) {
  if(device.platform.toLowerCase()=='android' && !auth.TokenID) {
    localStorage.clear();
    location.reload();
  }
  if(auth.RoleID!=ROLE_PUBLIK) {
    //$$('a.tab-link[href="#view-chat"]').hide();
    //$$('a.tab-link[href="#view-notification"]').show();
  }
}

$$("#view-home" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');

  //$$('a.tab-link[href="#view-chat"]').show();
  //$$('a.tab-link[href="#view-notification"]').hide();
  if (auth) {
    $$('.el-hide-login', ui.target).hide();
    $$('.el-show-login', ui.target).show();

    $$('[data-label=name]', ui.target).html(auth.Nama);
    $$('[data-label=email]', ui.target).html(auth.Role);

    if(auth.RoleID!=ROLE_PUBLIK) {
      //$$('a.tab-link[href="#view-chat"]').hide();
      //$$('a.tab-link[href="#view-notification"]').show();
    }
  }
});

$$("#view-chat" ).on('view:init', function(ui) {
  setInterval(function(){
    var auth = getLocalStorage('auth');
    var _uname = (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN'));

    getData('message-load'+(app.view.current.name=='chat'?'/1':''), {UserName: _uname}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        if(res.data) {
          var notifCount = 0;
          if(res.data.chats) {
            _loadChats(res.data.chats);
          }

          if(res.data.unread && res.data.unread.length > 0) {
            $$('[data-label=badge-chat]').addClass('badge').html(res.data.unread.length);
            notifCount += res.data.unread.length;
          } else {
            $$('[data-label=badge-chat]').removeClass('badge').html('');
          }

          if(res.data.notifs && res.data.notifs > 0) {
            $$('[data-label=badge-notif]').addClass('badge').html(res.data.notifs);
          } else {
            $$('[data-label=badge-notif]').removeClass('badge').html('');
          }

          if(res.data.notif1 && res.data.notif1 > 0) {
            $$('[data-label=badge-notif1]').addClass('badge').html(res.data.notif1);
            notifCount += res.data.notif1;
          } else {
            $$('[data-label=badge-notif1]').removeClass('badge').html('');
          }

          if(res.data.notif2 && res.data.notif2 > 0) {
            $$('[data-label=badge-notif2]').addClass('badge').html(res.data.notif2);
            notifCount += res.data.notif2;
          } else {
            $$('[data-label=badge-notif2]').removeClass('badge').html('');
          }

          /*console.log(notifCount);
          if(notifCount > 0) {
            cordova.plugins.notification.badge.set(notifCount);
          } else {
            cordova.plugins.notification.badge.clear();
          }*/
        }
      } else {
        app.dialog.alert(res.error);
        return false;
      }
    }, function(){
      var toast = app.toast.create({
        text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }, function(){

    });
  }, 2000);
});

$$("#view-chat" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');

  app.toolbar.hide("[data-label=toolbar-main]");

  $('[data-label=link-back]', ui.target).unbind('click').click(function(){
    var _viewHome = app.views.get("#view-home");
    app.tab.show("#view-home");
    app.toolbar.show("[data-label=toolbar-main]");
  });

  $('[data-label=btn-send]', ui.target).unbind('click').click(function(){
    var _from = (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN'));
    var _text = objMessagebar.getValue().replace(/\n/g, '<br>').trim(); //$$('[data-label=input-chat]', ui.target).val();

    if (!_text.length) return false;

    getData('message-send', {From: _from, Text: _text}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        objMessagebar.clear();
        objMessagebar.focus();

        if(res.data) {
          _loadChats(res.data);
        }
      } else {
        app.dialog.alert(res.error);
        return false;
      }
    }, function(){
      var toast = app.toast.create({
        text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }, function(){

    });
  });

  getData('message-read', {UserName: (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN'))}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {

    } else {
      app.dialog.alert(res.error);
      return false;
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){

  });
});

$$("#view-profile" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');
  $$('[data-label=input-name]', ui.target).val(auth.Nama);
  $$('[data-label=input-role]', ui.target).val(auth.Role);
  $$('[data-label=input-email]', ui.target).val(auth.Email);

  $('[data-label=btn-logout]', ui.target).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin logout dari aplikasi?', 'Logout', function(){
      localStorage.clear();
      location.reload();
    }, function(){

    });
    return false;
  });

  $('[data-label=btn-submit]', ui.target).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin memperbarui profil anda?', 'Profil', function(){
      var name = $$('[data-label=input-name]', ui.target).val();
      getData('changeprofile', {username: auth.Email, name: name}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          var toast = app.toast.create({
            text: res.success,
            icon: '<i class="icon material-icons md-only">check_circle</i>',
            position: 'center',
            closeTimeout: 2000
          });
          toast.open();
          setLocalStorage('auth', res.data);
          app.view.current.router.refreshPage();
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){
        app.preloader.hide();
      });
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:init', '.page[data-name="login"]', function (e, page) {
  var el = page.el;
  $('.button-login', el).unbind('click').click(function(){
    var username_ = $$('[name=username]', el).val();
    var password_ = $$('[name=password]', el).val();

    if(device.platform.toLowerCase()=='android') {
      cordova.plugins.firebase.messaging.getToken().then(function(token_) {
        console.log("Device Token: ", token_);
        //app.dialog.alert('Token = '+token_);
        app.preloader.show();
        getData('login', {username: username_, password: password_, token: token_}, function(res) {
          res = JSON.parse(res);
          if(res.error == 0) {
            setLocalStorage('auth', res.data);
            app.view.current.router.navigate('/',{reloadCurrent: true,ignoreCache: true,});
          }
          else {
            app.dialog.alert(res.error);
          }
        }, function() {
          app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
        }, function() {
          app.preloader.hide();
        });
      });
    } else {
      app.preloader.show();
      getData('login', {username: username_, password: password_}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          setLocalStorage('auth', res.data);
          app.view.current.router.navigate('/',{reloadCurrent: true,ignoreCache: true,});
        }
        else {
          app.dialog.alert(res.error);
        }
      }, function() {
        app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
      }, function() {
        app.preloader.hide();
      });
    }


  });
});

$$(document).on('page:beforein', '.page[data-name="home"]', function (e, page) {
  var el = page.el;
  var auth = getLocalStorage('auth');

  //$$('a.tab-link[href="#view-chat"]').show();
  //$$('a.tab-link[href="#view-notification"]').hide();
  if (auth) {
    $$('.el-hide-login', el).hide();
    $$('.el-show-login', el).show();

    $$('[data-label=name]', el).html(auth.Nama);
    $$('[data-label=email]', el).html(auth.Role);

    if(auth.RoleID!=ROLE_PUBLIK) {
      //$$('a.tab-link[href="#view-chat"]').hide();
      //$$('a.tab-link[href="#view-notification"]').show();
    }
  } else {
    $$('.el-hide-login', el).show();
    $$('.el-show-login', el).hide();
    //app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    //return false;
  }

  $('[data-label=link-chat]', el).unbind('click').click(function(){
    app.tab.show("#view-chat");
  });

  $('[data-label=btn-logout]', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin logout dari aplikasi?', 'Logout', function(){
      localStorage.clear();
      app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:init', '.page[data-name="register"]', function (e, page) {
  var el = page.el;
  $('.button-submit', el).unbind('click').click(function(){
    var _name = $$('[name=name]', el).val();
    var _phoneno = $$('[name=phoneno]', el).val();
    var _address = $$('[name=address]', el).val();
    var _password = $$('[name=password]', el).val();
    var _passwordcf = $$('[name=confirmpassword]', el).val();

    var valid = app.input.validateInputs($$('form', el));
    if(!valid) {
      app.dialog.alert('Harap isi form terlebih dahulu.');
      return false;
    }

    if(_password != _passwordcf) {
      app.dialog.alert('Harap konfirmasi password dengan benar.');
      return false;
    }

    app.preloader.show();
    getData('register', {name: _name, phoneno: _phoneno, password: _password, address: _address}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        var toast = app.toast.create({
          text: res.success,
          icon: '<i class="icon material-icons md-only">check_circle</i>',
          position: 'center',
          closeTimeout: 2000,
          on: {
            closed: function () {
              app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
            }
          }
        });
        toast.open();
      }
      else {
        app.dialog.alert(res.error);
      }
    }, function() {
      app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
    }, function() {
      app.preloader.hide();
    });
  });
});

$$(document).on('page:init', '.page[data-name="form"]', function (e, page) {
  var auth = getLocalStorage('auth');
  var param = page.route.params;
  var el = page.el;
  var formData = new FormData();

  var files_ = formData.getAll('file[]');

  $$('[name=input-tanggal]', el).val(moment().format("YYYY-MM-DD"));
  $('[data-label=text-attachment-count]', el).html(files_.length);
  if(files_ && files_.length > 0) {
    $('[data-label=btn-delete-attachment]', el).show();
  } else {
    $('[data-label=btn-delete-attachment]', el).hide();
  }

  $$('[data-label=btn-delete-attachment]', el).click(function() {
    app.dialog.confirm('Apakah anda yakin ingin menghapus lampiran?', 'Hapus Lampiran', function(){
      formData.delete('file[]');

      var files_ = formData.getAll('file[]');

      $('[data-label=text-attachment-count]', el).html(files_.length);
      if(files_ && files_.length > 0) {
        $('[data-label=btn-delete-attachment]', el).show();
      } else {
        $('[data-label=btn-delete-attachment]', el).hide();
      }
    });
  });

  $$('[data-label=btn-file-upload]', el).click(function(){
    //$('input[type=file][data-label=input-file]', el).click();
    var input_ = $('input[type=file][data-label=input-file-blueprint]', el).clone();
    var fileExist_ = formData.getAll('file[]');

    if(fileExist_ && fileExist_.length >= 4) {
      app.dialog.alert('Maaf, lampiran anda sudah mencapai kapasitas maksimum.');
      return false;
    }

    $('input[type=file][data-label=input-file]', el).remove();

    input_.attr('data-label', 'input-file').removeAttr('disabled');
    input_.appendTo('[data-label=list-attachment]');
    input_.click();

    input_.change(function(){
      formData.append("file[]", this.files[0]);

      var files_ = formData.getAll('file[]');

      $('[data-label=text-attachment-count]', el).html(files_.length);
      if(files_ && files_.length > 0) {
        $('[data-label=btn-delete-attachment]', el).show();
      } else {
        $('[data-label=btn-delete-attachment]', el).hide();
      }
    });
  });

  /*$('input[type=file][data-label=input-file]', el).change(function(){
    formData.delete("file");
    formData.append("file", this.files[0]);
  });*/

  if(param.kategori) {
    $$('[data-label=title]').html(param.kategori.toUpperCase());
    getData('get-opt-tipe/'+param.kategori, {}, function(res) {
      res = JSON.parse(res);
      if (res.error == 0) {
        var opt_ = '';

        if(res.data.opts && res.data.opts.length > 0) {
          opt_ += '<ul>';
          for(var i=0; i<res.data.opts.length; i++) {
            opt_ += '<li>';
            opt_ += '<label class="item-radio item-radio-icon-start item-content">';
            opt_ += '<input type="radio" name="input-tipe" value="'+res.data.opts[i].Value+'" '+(i==0?'checked':'')+' />';
            opt_ += '<i class="icon icon-radio"></i>';
            opt_ += '<div class="item-inner">';
            opt_ += '<div class="item-title">'+res.data.opts[i].Text+'</div>';
            opt_ += '</div>';
            opt_ += '</label>';
            opt_ += '</li>';
          }
          opt_ += '</ul>';
        } else {
          opt_ += '<ul>';
          opt_ += '<li>';
          opt_ += '<label class="item-radio item-radio-icon-start item-content">';
          opt_ += '<input type="radio" name="input-tipe" value="Lain-Lain" checked />';
          opt_ += '<i class="icon icon-radio"></i>';
          opt_ += '<div class="item-inner">';
          opt_ += '<div class="item-title">Lain-Lain</div>';
          opt_ += '</div>';
          opt_ += '</label>';
          opt_ += '</li>';
          opt_ += '</ul>';
        }

        $$('[data-label=list-opt-tipe]', el).removeClass('skeleton-text skeleton-effect-wave').html(opt_);
      }
    }, function() {}, function() {});
  }

  $('[data-label=btn-submit]', el).unbind('click').click(function(){
    var valid = app.input.validateInputs($$('form', el));
    if(!valid) {
      app.dialog.alert('Harap lengkapi form terlebih dahulu.');
      return false;
    }

    app.dialog.confirm('Apakah anda yakin ingin mengirimkan laporan / aduan ini?', 'Kirim', function(){
      //var file_ = $$('[name=input-file]').val();

      formData.append("LapKategori", param.kategori);
      formData.append("LapTipe", $$('[name=input-tipe]:checked', el).val());
      formData.append("LapJudul", $$('[name=input-judul]', el).val());
      formData.append("LapKeterangan", $$('[name=input-keterangan]', el).val());
      formData.append("LapLokasi", $$('[name=input-lokasi]', el).val());
      formData.append("LapTanggal", $$('[name=input-tanggal]', el).val());

      if(auth) formData.append("CreatedBy", (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN')));
      //if(file_) formData.append("file", $('input[type=file][data-label=input-file]', el).files[0]);

      app.preloader.show();
      app.request.post(URL_API+'laporan-add', formData).then(function (res) {
        app.preloader.hide();
        var res = JSON.parse(res.data);
        if(res.error == 0) {
          var toast = app.toast.create({
            text: res.success,
            icon: '<i class="icon material-icons md-only">check_circle</i>',
            position: 'center',
            closeTimeout: 2000
          });
          toast.open();
          app.view.current.router.navigate('/');
        } else {
          app.dialog.alert(res.error);
        }
      }).catch(function (err) {
        app.preloader.hide();
        app.dialog.alert(err.message);
      });
    });
  });
});

$$(document).on('page:init', '.page[data-name="list"]', function (e, page) {
  var auth = getLocalStorage('auth');
  var param = page.route.params;
  var el = page.el;

  if (auth) {
    if(auth.RoleID!=ROLE_PUBLIK) {
      $$('a[data-label="link-add"]').hide();
    }
  }

  if(param.kategori) {
    $$('[data-label=title]', el).html('DAFTAR '+param.kategori.toUpperCase());
    $$('[data-label=link-add]', el).attr('href', '/form/'+param.kategori+'/');
    $$('.item-media>img', el).attr('src', 'img/icon-'+param.kategori+'.png');

    getData('laporan/'+param.kategori,{UserName: (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN'))}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        $$('[data-label=list-laporan]', el).empty();
        $$('[data-label=list-laporan]', el).removeClass('skeleton-text skeleton-effect-wave');

        var arr = res.data;
        var html = '<ul>';
        if(arr && arr.length > 0) {
          for(var i=0; i<arr.length; i++) {
            html += '<li>';
            html += '<a href="/detail/'+arr[i].LapID+'/" class="item-link item-content">';
            html += '<div class="item-media"><img src="'+'img/icon-'+param.kategori+'.png'+'" width="44" /></div>';
            html += '<div class="item-inner">';
            html += '<div class="item-title-row"><div class="item-title">'+arr[i].LapJudul+'</div><div class="item-after">'+moment(arr[i].CreatedOn, "YYYY-MM-DD hh:mm:ss").format("DD/MM/YYYY")+'</div></div>';
            html += '<div class="item-subtitle" style="color: var(--f7-list-item-text-text-color) !important; font-size: 9pt !important">'+arr[i].LapStatus+'</div>';
            html += '<div class="item-text" style="color: var(--f7-list-item-text-subtitle-color) !important;">'+arr[i].LapTipe+'</div>';
            html += '</div>';
            html += '</a>';
            html += '</li>';
          }
        } else {
          html += '<li class="item-divider text-align-center">Belum ada data tersedia saat ini.</li>';
        }
        html += '</ul>';
        $$('[data-label=list-laporan]', el).html(html);
      }
    }, function(){}, function(){});
  }
});

$$(document).on('page:init', '.page[data-name="detail"]', function (e, page) {
  var auth = getLocalStorage('auth');
  var param = page.route.params;
  var el = page.el;

  if (auth) {
    if(auth.RoleID!=ROLE_PUBLIK) {
      $$('a[data-label="btn-log"]').attr('href', '/log/'+param.id+'/').show();
    }
  }

  getData('laporan-detail/'+param.id,{UserName: (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN'))}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      if(res.data.laporan) {
        $$('[data-label=lapid]', el).removeClass('skeleton-text skeleton-effect-wave').html('#'+res.data.laporan.LapID.padStart(4, '0'));
        $$('[data-label=lapjudul]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.laporan.LapJudul);
        $$('[data-label=lapkategori]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.laporan.LapKategori);
        $$('[data-label=lapstatus]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.laporan.LapStatus);
        $$('[data-label=lapketerangan]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.laporan.LapKeterangan);
        $$('[data-label=createdon]', el).removeClass('skeleton-text skeleton-effect-wave').html(moment(res.data.laporan.CreatedOn).format("DD/MM/YYYY"));
        $$('[data-label=icon]', el).attr('src', 'img/icon-'+res.data.laporan.LapKategori.toLowerCase()+'.png');
        /*if(res.data.laporan.LapFile) {
          $$('[data-label=img]', el).attr('src', res.data.laporan.LapFile).show();
        }*/
        if(res.data.laporan.LapFile) {
          $('[data-label=btn-attachment]', el).unbind('click').click(function(){
            var arrfile_ = res.data.laporan.LapFile.split(",");
            var files = [];

            for(var i=0; i<arrfile_.length; i++) {
              var ext = arrfile_[i].split('.').pop();
              if(ext=='mp4'||ext=='mov'||ext=='wmv'||ext=='avi'||ext=='flv'||ext=='mkv') {
                files.push('<video width="100%" controls><source src="'+arrfile_[i]+'" type="video/'+ext+'"></video>');
              } else {
                files.push(arrfile_[i]);
              }
            }

            var browser = app.photoBrowser.create({
              photos: files
            });
            browser.open();
            return false;
          });
        } else {
          $('[data-label=btn-attachment]', el).closest('div').hide();
        }
      }

      var htmlLogs = '';
      if(res.data.logs && res.data.logs.length > 0) {
        var arrLogs = res.data.logs;
        for(var i=0; i<arrLogs.length; i++) {
          var name_ = arrLogs[i].LogName;
          if(name_ == auth.Email) name_ = 'Saya';

          htmlLogs += '<div class="timeline-item">';
          htmlLogs += '<div class="timeline-item-divider"></div>';
          htmlLogs += '<div class="timeline-item-content">';
          htmlLogs += '<div class="timeline-item-inner">';
          htmlLogs += '<div class="timeline-item-time">'+moment(arrLogs[i].LogTimestamp).format("DD-MM-YYYY hh:mm")+'</div>';
          htmlLogs += '<div class="timeline-item-title padding-top-half">'+arrLogs[i].LogDescription+'</div>';
          if(arrLogs[i].LogFile) {
            htmlLogs += '<div class="timeline-item-text padding-top-half" style="font-size: var(--f7-timeline-item-time-font-size) !important; color: var(--f7-timeline-item-time-text-color) !important;"><a class="link" href="'+arrLogs[i].LogFile+'" data-label="btn-log-media"><i class="far fa-link"></i>&nbsp;LAMPIRAN</a></div>';
          }
          htmlLogs += '<div class="timeline-item-text padding-top-half" style="font-size: var(--f7-timeline-item-time-font-size) !important; color: var(--f7-timeline-item-time-text-color) !important; font-style: italic !important; text-align: right !important">'+name_+'</div>';
          htmlLogs += '</div>';
          htmlLogs += '</div>';
          htmlLogs += '</div>';
        }
      } else {
        htmlLogs += '<div class="timeline-item">';
        //htmlLogs += '<div class="timeline-item-date">21 <small>DEC</small></div>';
        htmlLogs += '<div class="timeline-item-divider"></div>';
        htmlLogs += '<div class="timeline-item-content">KOSONG</div>';
        htmlLogs += '</div>';
      }
      $$('[data-label=riwayat]', el).removeClass('skeleton-text skeleton-effect-wave').html(htmlLogs);
      $('[data-label=btn-log-media]', el).click(function(){
        var arrfile_ = $(this).attr('href').split(",");
        var files = [];

        for(var i=0; i<arrfile_.length; i++) {
          var ext = arrfile_[i].split('.').pop();
          if(ext=='mp4'||ext=='mov'||ext=='wmv'||ext=='avi'||ext=='flv'||ext=='mkv') {
            files.push('<video width="100%" controls><source src="'+arrfile_[i]+'" type="video/'+ext+'"></video>');
          } else {
            files.push(arrfile_[i]);
          }
        }

        var browser = app.photoBrowser.create({
          photos: files
        });
        browser.open();
        return false;
      });
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){});
});

$$(document).on('page:init', '.page[data-name="notification"]', function (e, page) {
  var auth = getLocalStorage('auth');
  var param = page.route.params;
  var el = page.el;

  getData('notification-load/'+(auth && auth.RoleID!=ROLE_PUBLIK?'opr':'public'),{UserName: auth?auth.Email:'guest'}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      var notifs = '';
      notifs += '<ul>';
      if(res.data && res.data.length>0) {
        for(var i=0; i<res.data.length; i++) {
          notifs += '<li>';
          notifs += '<a href="/detail/'+res.data[i].ID+'/" class="item-link item-content" data-force="true" data-ignore-cache="true">';
          notifs += '<div class="item-media">';
          notifs += '<img src="img/icon-'+res.data[i].kat+'.png" width="44" />';
          notifs += '</div>';
          notifs += '<div class="item-inner">';
          notifs += '<div class="item-title-row">';
          notifs += '<div class="item-title">'+res.data[i].title+'</div>';
          notifs += '<div class="item-after">'+res.data[i].subtitle+'</div>';
          notifs += '</div>';
          //notifs += '<div class="item-subtitle">'+res.data[i].subtitle+'</div>';
          notifs += '<div class="item-text">'+res.data[i].text+'</div>';
          notifs += '</div>';
          notifs += '</a>';
          notifs += '</li>';
        }
      } else {
        notifs += '<li class="text-align-center" style="padding: 10px !important">KOSONG</li>';
      }
      $$('[data-label=list-notification]', el).removeClass('skeleton-text skeleton-effect-wave').html(notifs);
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){});
});

$$(document).on('page:init', '.page[data-name="log"]', function (e, page) {
  var auth = getLocalStorage('auth');
  var param = page.route.params;
  var el = page.el;
  var formData = new FormData();
  var files_ = formData.getAll('file[]');

  $('[data-label=text-attachment-count]', el).html(files_.length);
  if(files_ && files_.length > 0) {
    $('[data-label=btn-delete-attachment]', el).show();
  } else {
    $('[data-label=btn-delete-attachment]', el).hide();
  }

  $$('[data-label=btn-delete-attachment]', el).click(function() {
    app.dialog.confirm('Apakah anda yakin ingin menghapus lampiran?', 'Hapus Lampiran', function(){
      formData.delete('file[]');

      var files_ = formData.getAll('file[]');

      $('[data-label=text-attachment-count]', el).html(files_.length);
      if(files_ && files_.length > 0) {
        $('[data-label=btn-delete-attachment]', el).show();
      } else {
        $('[data-label=btn-delete-attachment]', el).hide();
      }
    });
  });

  $$('[data-label=btn-file-upload]', el).click(function(){
    var input_ = $('input[type=file][data-label=input-file-blueprint]', el).clone();
    var fileExist_ = formData.getAll('file[]');

    if(fileExist_ && fileExist_.length >= 4) {
      app.dialog.alert('Maaf, lampiran anda sudah mencapai kapasitas maksimum.');
      return false;
    }

    $('input[type=file][data-label=input-file]', el).remove();

    input_.attr('data-label', 'input-file').removeAttr('disabled');
    input_.appendTo('[data-label=list-attachment]');
    input_.click();

    input_.change(function(){
      formData.append("file[]", this.files[0]);

      var files_ = formData.getAll('file[]');

      $('[data-label=text-attachment-count]', el).html(files_.length);
      if(files_ && files_.length > 0) {
        $('[data-label=btn-delete-attachment]', el).show();
      } else {
        $('[data-label=btn-delete-attachment]', el).hide();
      }
    });
  });

  $('[data-label=btn-submit]', el).unbind('click').click(function(){
    var valid = app.input.validateInputs($$('form', el));
    if(!valid) {
      app.dialog.alert('Harap lengkapi form terlebih dahulu.');
      return false;
    }

    app.dialog.confirm('Apakah anda yakin ingin menambahkan catatan ini?', 'Kirim', function(){
      //var file_ = $$('[name=input-file]').val();

      formData.append("LapID", param.id);
      formData.append("LogTipe", 'CATATAN');
      formData.append("LogRemarks", $$('[name=input-keterangan]').val());

      if(auth) formData.append("LogUserName", (auth?auth.Email:(device.uuid?device.uuid:'UNKNOWN')));
      //if(file_) formData.append("file", $('input[type=file][data-label=input-file]', el).files[0]);

      app.preloader.show();
      app.request.post(URL_API+'log-add/'+param.id, formData).then(function (res) {
        app.preloader.hide();
        var res = JSON.parse(res.data);
        if(res.error == 0) {
          var toast = app.toast.create({
            text: res.success,
            icon: '<i class="icon material-icons md-only">check_circle</i>',
            position: 'center',
            closeTimeout: 2000
          });
          toast.open();
          app.view.current.router.navigate('/detail/'+param.id+'/',{reloadCurrent: true});
        } else {
          app.dialog.alert(res.error);
        }
      }).catch(function (err) {
        app.preloader.hide();
        app.dialog.alert(err.message);
      });
    });
  });
});
