routes = [
  /*{
    path: '/tabs/',
    tabs: [
      {
        path: '/',
        id: 'view-home'
      },
      {
        path: '/catalog/',
        id: 'view-catalog'
      },
      {
        path: '/order/',
        id: 'view-order'
      },
    ],
  },*/
  {
    name:'home',
    path: '/',
    url: './index.html'
  },
  {
    name:'login',
    path: '/login/',
    url: './pages/login.html'
  },
  {
    name:'register',
    path: '/register/',
    url: './pages/register.html'
  },
  {
    name:'notification',
    path: '/notification/',
    url: './pages/notification.html'
  },
  {
    name:'form',
    path: '/form/:kategori/',
    url: './pages/form.html'
  },
  {
    name:'list',
    path: '/list/:kategori/',
    url: './pages/list.html'
  },
  {
    name:'detail',
    path: '/detail/:id/',
    url: './pages/detail.html'
  },
  {
    name:'log',
    path: '/log/:id/',
    url: './pages/log.html'
  },
  // Default route (404 page). MUST BE THE LAST
  /*{
      path: '(.*)',
      url: './pages/404.html'
  },*/
];
